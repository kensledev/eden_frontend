


/* eslint-disable-no-console */


// ----------------------------------------------------=|Imports|=-|
  import Vue from 'vue'
  import App from './components/app.vue'
  import router from './router'

  import MainView from './components/mainView.vue'
  import Timer from './components/timer.vue'
  import Machine from './components/machine.vue'
  import Modal from './components/modal.vue'
  import Database from './components/database.vue'
  import CustomerSearch from './components/customersearch.vue'

// ----------------------------------------------------=|Components|=-|

  Vue.component('main-view', MainView)
  Vue.component('timer', Timer)
  Vue.component('machine', Machine)
  Vue.component('modal', Modal)
  Vue.component('database', Database)
  Vue.component('customersearch', CustomerSearch)

// ----------------------------------------------------=|Plugins|=-|
  import VeeValidate from 'vee-validate'
  import VueSocketio from 'vue-socket.io'
  // import AnimatedVue from 'animated-vue'
  // import VueGesture from 'vue-gesture' 
  
  // Vue.use(VueGesture)
  // Vue.use(AnimatedVue) 
  Vue.use(VeeValidate)
  // Vue.use(VueSocketio, 'http://jquinandy.pro:5000')
  Vue.use(VueSocketio, 'https://edenapp.eu:5001', {secure: true})
  

// ----------------------------------------------------=|Custom Directives|=-|

  Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
    inserted: function (el) {
    // Focus the element
      el.focus();
      el.select()
    }
  })


// ----------------------------------------------------=|Innit App|=-|
/* eslint-disable no-new */
/* eslint-disable no-unused-vars */

export const bus = new Vue({});

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  render: h => h(App)
})


