// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: [ 
    'standard',
    'eslint:recommended',
    // 'plugin:vue/recommended' // or 'plugin:vue/base'
  ],
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  'rules': {
    // allow paren-less arrow functions
    // 'vue/valid-v-if': 'error',
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    'no-console': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    // // Trailing white spaces
    // "skipBlankLines": true
  }
}
