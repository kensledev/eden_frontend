FROM node:0.12.7

RUN npm install webpack -g

FROM node

ENV NPM_CONFIG_LOGLEVEL warn
ARG app_env
ENV APP_ENV $app_env

RUN mkdir -p usr/src/frontend
WORKDIR usr/src/frontend
COPY . ./

RUN npm install

CMD if [ ${APP_ENV} = production ]; \
	then \
	npm run build; \
	else \
	npm run dev; \
	fi

EXPOSE 8080
