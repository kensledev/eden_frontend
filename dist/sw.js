self.addEventListener('push', function (event) {
  // console.log('[Service Worker] Push Received.')
  // console.log(`[Service Worker] Push had this data: "${event.data.text()}"`)
  // console.log(`[Service Worker] Push had this data: "${event.data}"`)

  // console.log(event.data)

  var messageBody = event.data.text();

  if (messageBody.split(' ')[0] === "Timer") {
    var tempTitle = messageBody.split(' ')[0] + ' ' + messageBody.split(' ')[1];
    var message = event.data.text().substring(tempTitle.length)
    console.log(message);
  }

  const title = tempTitle || "Not assigned"
  const options = {
    body: message,
    icon: 'static/img/icons/android-chrome-192x192.png',
    badge: 'static/img/icons/android-chrome-512x512.png'
  }

  const notificationPromise = self.registration.showNotification(title, options)
  event.waitUntil(notificationPromise)
})

// self.addEventListener('notificationclick', function (event) {
//   console.log('[Service Worker] Notification click Received.')

//   event.notification.close()

//   console.log('clicked notification')

//   // event.waitUntil(
//   //   clients.openWindow('https://developers.google.com/web/')
//   // )
// })
