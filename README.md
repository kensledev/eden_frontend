# Eden


Responsive PWA table booking application, Made originally for a gaming cafe.

- A timer per table
- Push notifications 
- Responive design - emulates floor plan when landscape
- Express / Docker backend https://bitbucket.org/kensledev/eden_backend/src/master/
- JWT Authentication


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
